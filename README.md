# FoundryVTT DnD5e module CZ compendium - BETA

Modul obsahuje český překlad kompendia pro dnd5e system v [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop")

Texty byly přezvaty z českého (SRD) překladu a data a statistiky přeloženy pomocí [babele modulu](https://gitlab.com/riccisi/foundryvtt-babele) .

Script převzdat a upraven od italských kolegů: https://gitlab.com/riccisi/foundryvtt-dnd5e-lang-it-it/

Kompendium obsahuje tyto seznamy:
* Bestiář (SRD)
* Kouzla (SRD)
* Obchodní zboží (SRD)
* Předměty (SRD)
* Povolání (SRD)
* Schopnost povolání (SRD)
* Rasové rysy (SRD)
* Startovní hrdiny (SRD)

### Instalace

V hlavním menu kliknout na 'Add-On Modules' a poté kliknout 'Install Module' a dole do 'Manifest URL' vložit adresu níže.

https://gitlab.com/lehovec/foundryvtt-dnd5e-compendium-cz/-/raw/master/module.json

Nezapomeň aktivovat tento modul a babel modul!

### Užitečné odkazy

Doporučuji používat překladové moduly pro [FoundryVTT](http://foundryvtt.com/ "Foundry Virtual Tabletop") pro plný zážitek v češtině:

- [FoundryVTT lang cs-CZ](https://gitlab.com/ptoseklukas/foundryvtt-lang-cs-cz)
- [FoundryVTT dnd5e lang cs-CZ](https://gitlab.com/ptoseklukas/foundryvtt-dnd5e-lang-cs-cz)