
var types = {
	"aberration (shapechanger)":"Zrůda (tvaroměnec)",
	"aberration":"Zrůda",
	"beast":"Zvíře",
	"celestial (titan)":"Nebešťan (titán)",
	"celestial":"Nebešťan",
	"construct":"Výtvor",
	"dragon":"Drak",
	"elemental":"Elementál",
	"fey":"Víla",
	"fiend (demon)":"Běs (démon)",
	"fiend (demon, orc)":"Běs (démon, ork)",
	"fiend (demon, shapechanger)":"Běs (demone, tvaroměnec)",
	"fiend (devil)":"Běs (ďábel)",
	"fiend (devil, shapechanger)":"Běs (ďábel, tvaroměnec)",
	"fiend (gnoll)":"Běs (gnol)",
	"fiend (shapechanger)":"Běs (tvaroměnec)",
	"fiend (yugoloth)":"Běs (yugoloth)",
	"fiend":"Běs",
	"giant (cloud giant)":"Obr (obr, oblačný)",
	"giant (fire giant)":"Obr (obr, ohnivý)",
	"giant (frost giant)":"Obr (obr, ledový)",
	"giant (hill giant)":"Obr (obr, kopcový)",
	"giant (stone giant)":"Obr (obr, kamenný)",
	"giant (storm giant)":"Obr (obr, bouřný)",
	"giant":"Obr",
	"humanoid (aarakocra)":"Humanoid (árakokra)",
	"humanoid (any race)":"Humanoid (jakákoli rasa)",
	"humanoid (bullywug)":"Humanoid (ropušák)",
	"humanoid (dwarf)":"Humanoid (trpaslík)",
	"humanoid (elf)":"Humanoid (elf)",
	"humanoid (firenewt)":"Humanoid (ohněmlok)",
	"humanoid (gith)":"Humanoid (gith)",
	"humanoid (gnoll)":"Humanoid (gnol)",
	"humanoid (gnome)":"Humanoid (gnóm)",
	"humanoid (goblinoid)":"Humanoid (skřet)",
	"humanoid (grimlock)":"Humanoid (divous)",
	"humanoid (grung)":"Humanoid (grung)",
	"humanoid (human)":"Humanoid (člověk)",
	"humanoid (human, shapechanger)":"Humanoid (člověk, tvaroměnec)",
	"humanoid (kenku)":"Humanoid (kenku)",
	"humanoid (kobold)":"Humanoid (kobold)",
	"humanoid (kuo-toa)":"Humanoid (kuo-toa)",
	"humanoid (lizardfolk)":"Humanoid (ještěrec)",
	"humanoid (merfolk)":"Humanoid (mořan)",
	"humanoid (orc)":"Humanoid (ork)",
	"humanoid (quaggoth)":"Humanoid (quagot)",
	"humanoid (sahuagin)":"Humanoid (sahuagin)",
	"humanoid (shapechanger)":"Humanoid (tvaroměnec)",
	"humanoid (thri-kreen)":"Humanoid (thri-kreen)",
	"humanoid (troglodyte)":"Humanoid (troglodyt)",
	"humanoid (xvart)":"Humanoid (xvart)",
	"humanoid (yuan-ti)":"Humanoid (yuan-ti)",
	"humanoid":"Humanoid",
	"monstrosity (shapechanger)":"Obluda (tvaroměnec)",
	"monstrosity (shapechanger, yuan-ti)":"Obluda (tvaroměnec, yuan-ti)",
	"monstrosity (titan)":"Obluda (titán)",
	"monstrosity":"Obluda",
	"ooze":"Sliz",
	"plant":"Rostlina",
	"swarm of Tiny beasts":"hejno Drobných zvířat",
	"undead (shapechanger)":"nemrtvý (tvaroměnec)",
	"undead":"nemrtvý"
};

var alignments = {
	"chaotic evil": "Chaotické zlo",
	"chaotic neutral":"Chaoticky neutrální",
	"chaotic good":"Chaotické dobro",
	"neutral evil":"Neutrální zlo",
	"true neutral":"Neutrální",
	"neutral":"Neutrální",
	"neutral good":"Neutrální dobro",
	"lawful evil":"Zákonné zlo",
	"lawful neutral":"Zákonně neutrální",
	"lawful good":"Zákonné dobro",
	"chaotic good evil":"Chaotické dobro/zlo",
	"lawful chaotic evil":"Zákonné/Chaotické zlo",
	"unaligned":"Bez přesvědčení",
	"any non-lawful": "Jakékoli nezákonné",
	"any": "Jakékoli",
};

var languages = {
	"giant eagle": "řeč obřích orlů",
	"worg":"řeč vrrků",
	"winter wolf":"řeč sněžných vlků",
	"sahuagin":"sahuaginština",
	"giant owl, understands but cannot speak all but giant owl":"řeč obřích sov, rozumí ostatním jazykům, ale neumí jimi mluvit",
	"giant elk but can't speak them":"řeč obřích losů, rozumí ostatním jazykům, ale neumí jimi mluvit",
	"understands infernal but can't speak it":"rozumí ďábelštině, ale neumí jí mluvit",
	"understands draconic but can't speak":"rozumí dračí řeči, ale neumí jí mluvit",
	"understands common but doesn't speak it":"rozumí obecné řeči, ale neumí jí mluvit",
	"understands abyssal but can't speak":"rozumí démonština, ale neumí jí mluvit",
	"understands all languages it knew in life but can't speak":"rozumí všem jazykům které umí, ale neumí jimi mluvit",
	"understands commands given in any language but can't speak":"rozumí všem příkazům v jakémkoli jazyce, ale neumí jím mluvit",
	"(can't speak in rat form)":"(nemůže mluvit v krysí podobě)",
	"(can't speak in boar form)":"(nemůže mluvit v kančí podobě)",
	"(can't speak in bear form)":"(nemůže mluvit v medvědí podobě)",
	"(can't speak in tiger form)":"(nemůže mluvit v tygří podobě)",
	"any one language (usually common)":"jakýkoli jeden další jazyk, obvykle obecná řeč",
	"any two languages":"jakýkoli dva další jazyky",
	"any four languages":"jakýkoli čtyři další jazyky",
	"5 other languages":"ř dalších jazyků",
	"any, usually common":"jakýkoli, obvykle obecná řeč",
	"one language known by its creator":"rozumí jazykům svého tvůrce",
	"the languages it knew in life":"rozumí jazykům, které znal za života",
	"those it knew in life":"rozumí jazykům, které znal za života",
	"all it knew in life":"rozumí jazykům, které znal za života",
	"any it knew in life":"rozumí jazykům, které znal za života",
	"all, telepathy 120 ft.":"jakýkoli, telepatie 120stop",
	"telepathy 60 ft.":"telepatie 60stop",
	"telepathy 60ft. (works only with creatures that understand abyssal)":"telepatie 60stop (funguje jen s tvory, kteří rozumí démonštině)",
	"telepathy 120 ft.":"telepatie 120stop",
	"but can't speak":"ale neumí mluvit",
	"but can't speak it":"ale neumí jim mluvit",
	"choice":"dle volby",
	"understands the languages of its creator but can't speak":"rozumí jazykům svého tvůrce, ale neumí jím mluvit",
	"understands common and giant but can't speak":"rozumí obecné řeči a gigantštině, ale neumí jimi mluvit",
	"cannot speak": "neumí mluvit"
};

var races = {
	"Dragonborn": "Drakorozený",
	"Dwarf": "Trpaslík",
	"Hill Dwarf": "Kopcový trpaslík",
	"Elf": "Elf",
	"High Elf": "Vznešený elf",
	"Gnome": "Gnóm",
	"Rock Gnome": "Skalní gnóm",
	"Half Elf": "Půlelf",
	"Half-elf": "Půlelf",
	"Halfling": "Hobit",
	"Lightfoot Halfling": "Tichošlápek",
	"Half Orc": "Půlork",
	"HUMAN": "Člověk",
	"Human": "Člověk",
	"Variant Human": "Varianta Člověka",
	"Tiefling": "Tiefling"
};

var rarity = {
	"Common": "Běžný",
	"Uncommon": "Neobvyklý",
	"Rare": "Vzácný",
	"Very rare": "Velmi vzácný",
	"Legendary": "Legendární"
};

function round(num) {
	return Math.round((num + Number.EPSILON) * 100) / 100;
}

function lbToKg(lb) {
	return parseInt(lb)/2;
}

function footsToFathoms(ft) {
	return round(parseInt(ft)*(1/6));
}

function milesToMeters(mi) {
	return round(parseInt(mi)*1.5);
}

function parseSenses(sensesText) {
	const senses = sensesText.split('. ');
	let parsed = '';
	senses.forEach(sense => { parsed = parseSense(sense) + ' ' + parsed; });
	return parsed;
}

function parseSense(sense) {
	var regexp = /([0-9]+)/gi;
	sense = sense.replace(/ft/gi, 'st');
	sense = sense.replace(/feet/gi, 'stop');
	sense = sense.replace(/Darkvision/gi, "Vidění ve tmě");
	sense = sense.replace(/Darvision/gi, "Vidění ve tmě"); //bug ^^
	sense = sense.replace(/Blindsight/gi, "Mimozrakové vnímání");
	sense = sense.replace(/Truesight/gi, "Pravdivé vidění");
	sense = sense.replace(/tremorsense/gi, "Citlivost na otřesy");
	sense = sense.replace(/Blind Beyond/gi, "slepý nad");
	sense = sense.replace(/this radius/gi, "tento okruh");
	sense = sense.replace((sense.match(regexp)), footsToFathoms(sense.match(regexp)));
	sense = sense.replace("(blind beyond this radius)", "(slepý přes tuto hodnotu)");
	return sense;
}

function parseDamage(damage) {
	damage = damage.replace(/bludgeoning/gi, 'drtivé ');
	damage = damage.replace(/piercing/gi, 'bodné');
	damage = damage.replace(/and/gi, 'a');
	damage = damage.replace(/slashing/gi, 'sečné');
	damage = damage.replace(/from/gi, 'z');
	damage = damage.replace(/nonmagical attacks/gi, 'nemagický útok');
	damage = damage.replace(/that aren't silvered/gi, 'jež není postříbřený');
	damage = damage.replace(/not made with silvered weapons/gi, 'jež není vyrobený ze stříbré zbraně');
	return damage;
}

function convertEnabled() {
	return game.settings.get("compendium-dnd5e-cz", "convert");
}

function setUnitLabels() {
	let convert = convertEnabled();

	mergeObject(CONFIG.DND5E.distanceUnits, {
		"ft": convert ? "sáhy": "stopy",
		"mi": convert ? "kilometry": "míle"
	});

	if(convert) {
		CONFIG.DND5E.movementUnits = {
			"sáhy": "sáhy",
			"km": "Km"
		}
	} else {
		CONFIG.DND5E.movementUnits = {
			"ft": "stopy",
			"mi": "míle"
		}
	}
}

function setEncumbranceData() {
	let convert = convertEnabled();
	CONFIG.DND5E.encumbrance.currencyPerWeight = convert ? 100 : 50;
	CONFIG.DND5E.encumbrance.strMultiplier = convert ? 7.5 : 15;
}

Hooks.once('init', () => {

	if(typeof Babele !== 'undefined') {

		game.settings.register("compendium-dnd5e-cz", "convert", {
			name: "Automaticky převést jednoty",
			hint: "Převede jednotky na sáhy, kilometry, a kilogramy",
			scope: "world",
			type: Boolean,
			default: false,
			config: true,
			onChange: convert => {
				setUnitLabels();
				setEncumbranceData();
			}
		});

		Babele.get().register({
			module: 'compendium-dnd5e-cz',
			lang: 'cs',
			dir: 'compendium'
		});

		Babele.get().registerConverters({
			"weight": (value) => {

				if(!convertEnabled()) {
					return value;
				}

				return lbToKg(value);
			},
			"range": (range) => {
				if(range) {

					if(!convertEnabled()) {
						return range;
					}

					if(range.units === 'ft') {
						if(range.long) {
							range = mergeObject(range, { long: footsToFathoms(range.long) });
						}
						return mergeObject(range, { value: footsToFathoms(range.value) });
					}
					if(range.units === 'mi') {
						if(range.long) {
							range = mergeObject(range, { long: milesToMeters(range.long) });
						}
						return mergeObject(range, { value: milesToMeters(range.value) });
					}
					return range;
				}
			},
			"alignement": (alignment) => {
				return alignments[alignment.toLowerCase()];
			},
			"movement": (movement) => {

				if(!convertEnabled()) {
					return movement;
				}

				let convert = (value) => { return value; };
				let units = movement.units;
				if(units === 'ft') {
					convert = (value) => { return footsToFathoms(value) };
					units = "sáhy";
				}
				if(units === 'ml') {
					convert = (value) => { return footsToFathoms(value) };
					units = "sáhy";
				}

				return mergeObject(movement, {
					burrow: convert(movement.burrow),
					climb: convert(movement.climb),
					fly: convert(movement.fly),
					swim: convert(movement.swim),
					units: units,
					walk: convert(movement.walk)
				});
			},
			"senses": (senses) => {
				return senses ? parseSenses(senses) : null;
			},
			"di": (damage) => {
				return parseDamage(damage);
			},
			"languages": (lang) => {
				if (lang != null ) {
					const languagesSplit = lang.split('; ');
					var languagesFin = '';
					var languagesTr = '';
					languagesSplit.forEach(function(el){
						languagesTr = languages[el.toLowerCase()] ;
						if (languagesTr != null) {
							if (languagesFin === '') {
								languagesFin = languagesTr;
							}  else {
								languagesFin = languagesFin + ' ; '  + languagesTr;
							}
						}
					});
					return languagesFin;
				}
			},
			"token": (token) => {
				mergeObject(
					token, {
						dimSight: footsToFathoms(token.dimSight),
						brightSight: footsToFathoms(token.brightSight)
					}
				);
			},
			"race": (race) => {
				return races[race] ? races[race] : race;
			},
			"rarity": (r) => {
				return rarity[r] ? rarity[r] : r
			}
		});

		CONFIG.DND5E.classFeatures = {
			"barbar": CONFIG.DND5E.classFeatures["barbarian"],
			"bard": CONFIG.DND5E.classFeatures["bard"],
			"klerik": CONFIG.DND5E.classFeatures["cleric"],
			"druid": CONFIG.DND5E.classFeatures["druid"],
			"bojovník": CONFIG.DND5E.classFeatures["fighter"],
			"mnich": CONFIG.DND5E.classFeatures["monk"],
			"paladin": CONFIG.DND5E.classFeatures["paladin"],
			"hraničář": CONFIG.DND5E.classFeatures["ranger"],
			"tulák": CONFIG.DND5E.classFeatures["rogue"],
			"čaroděj": CONFIG.DND5E.classFeatures["sorcerer"],
			"černokněžník": CONFIG.DND5E.classFeatures["warlock"],
			"kouzelník": CONFIG.DND5E.classFeatures["wizard"]
		};
	}
});

Hooks.once('ready', () => {
	setUnitLabels();
	setEncumbranceData();
});

Hooks.on('preCreateScene', (scene) => {
	if(convertEnabled()) {
		mergeObject(scene, { "gridUnits": "sáhy", "gridDistance": 1.5 });
	}
});

Hooks.on('createActor', (actor) => {
	if(actor.getFlag("babele", "translated")) {
		return;
	}
	if(convertEnabled()) {
		actor.update({
			 token: {
				 dimSight: footsToFathoms(actor.data.token.dimSight),
				 brightSight: footsToFathoms(actor.data.token.brightSight)
			 },
			 data: {
				 attributes: {
					 movement: {
						 burrow: 0,
						 climb: 0,
						 fly: 0,
						 swim: 0,
						 units: 'sáhy',
						 walk: 9
					 }
				 }
			 }
		 });
	}
})

async function skillSorting() {
	const lists = document.getElementsByClassName("skills-list");
	for (let list of lists) {
		const competences = list.childNodes;
		let complist = [];
		for (let sk of competences) {
			if (sk.innerText && sk.tagName == "LI") {
				complist.push(sk);
			}
		}
		complist.sort(function(a, b) {
			return (a.innerText > b.innerText) ? 1 : -1;
		});
		for (let sk of complist) {
			list.appendChild(sk)
		}
	}
}

Hooks.on("renderActorSheet", async function() {
	skillSorting();
});